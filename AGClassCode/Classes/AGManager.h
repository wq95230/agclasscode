//
//  AGManager.h
//  BaseDemo
//
//  Created by 刘剑锋 on 2022/3/10.
//  Copyright © 2022 Liujianfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AGManager : NSObject

- (void)testPrint:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
