//
//  AGAppDelegate.h
//  AGClassCode
//
//  Created by wq95230 on 03/12/2022.
//  Copyright (c) 2022 wq95230. All rights reserved.
//

@import UIKit;

@interface AGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
