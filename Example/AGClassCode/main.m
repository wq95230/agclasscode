//
//  main.m
//  AGClassCode
//
//  Created by wq95230 on 03/12/2022.
//  Copyright (c) 2022 wq95230. All rights reserved.
//

@import UIKit;
#import "AGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AGAppDelegate class]));
    }
}
